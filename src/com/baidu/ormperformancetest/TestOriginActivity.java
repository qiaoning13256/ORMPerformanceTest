package com.baidu.ormperformancetest;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.baidu.ormperformancetest.origin.OriginSimpleDataDAO;
import com.baidu.ormperformancetest.origin.SimpleData;

public class TestOriginActivity extends BaseTestActivity<SimpleData> {
	
	public static final void launch(final Context context) {
		final Intent intent = new Intent(context, TestOriginActivity.class);
		context.startActivity(intent);
	}
	
	private OriginSimpleDataDAO originSimpleDataDAO = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_origin);
		
		originSimpleDataDAO = new OriginSimpleDataDAO(TestOriginActivity.this);
		
		new PerformanceTestTask().execute();
	}

	@Override
	protected void deleteAll() {
		originSimpleDataDAO.deleteAll();
	}

	@Override
	protected List<SimpleData> prepareTestData(int count) {
		final List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
		for(int i = 0; i < count; i++) {
			final SimpleData simpleData = new SimpleData();
			simpleData.setName("Name" + i);
			simpleDatas.add(simpleData);
		}
		return simpleDatas;
	}

	@Override
	protected void oneByOneInsert(SimpleData object) {
		originSimpleDataDAO.insertStatement(object);
	}

	@Override
	protected void oneByOneUpdate(SimpleData object) {
		originSimpleDataDAO.update(object);
	}

	@Override
	protected void batchInsert(List<SimpleData> objects) {
		originSimpleDataDAO.insertAllStatement(objects);
	}

	@Override
	protected void batchUpdate(List<SimpleData> objects) {
		originSimpleDataDAO.updateAll(objects);
	}

	@Override
	protected void reload() {
		final List<SimpleData> simpleDatas = originSimpleDataDAO.loadAll();
	}

	@Override
	protected String getLogTagName() {
		return "Origin";
	}

}
