package com.baidu.ormperformancetest;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.baidu.ormperformancetest.greendao.DaoMaster;
import com.baidu.ormperformancetest.greendao.DaoMaster.DevOpenHelper;
import com.baidu.ormperformancetest.greendao.DaoSession;
import com.baidu.ormperformancetest.greendao.SimpleData;
import com.baidu.ormperformancetest.greendao.SimpleDataDao;

public class TestGreenDAOActivity extends BaseTestActivity<SimpleData> {
	
	public static final void launch(final Context context) {
		final Intent intent = new Intent(context, TestGreenDAOActivity.class);
		context.startActivity(intent);
	}
	
	private SimpleDataDao simpleDataDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_greendao);
		
		final DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(TestGreenDAOActivity.this, "test-greendao-db", null);
		final SQLiteDatabase db = devOpenHelper.getWritableDatabase();
		final DaoMaster daoMaster = new DaoMaster(db);
		final DaoSession daoSession = daoMaster.newSession();
		simpleDataDao = daoSession.getSimpleDataDao();
		
		new PerformanceTestTask().execute();
	}

	@Override
	protected void deleteAll() {
		simpleDataDao.deleteAll();
	}

	@Override
	protected List<SimpleData> prepareTestData(int count) {
		final List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
		for(int i = 0; i < count; i++) {
			final SimpleData simpleData = new SimpleData();
			simpleData.setName("Name" + i);
			simpleDatas.add(simpleData);
		}
		return simpleDatas;
	}

	@Override
	protected void oneByOneInsert(SimpleData object) {
		simpleDataDao.insert(object);
	}

	@Override
	protected void oneByOneUpdate(SimpleData object) {
		simpleDataDao.update(object);
	}

	@Override
	protected void batchInsert(List<SimpleData> objects) {
		simpleDataDao.insertInTx(objects);
	}

	@Override
	protected void batchUpdate(List<SimpleData> objects) {
		simpleDataDao.updateInTx(objects);
	}

	@Override
	protected void reload() {
		final List<SimpleData> simpleDatas = simpleDataDao.loadAll();
	}

	@Override
	protected String getLogTagName() {
		return "GreenDAO";
	}

}
