package com.baidu.ormperformancetest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.baidu.ormperformancetest.ormlite.ORMLiteDBHelper;
import com.baidu.ormperformancetest.ormlite.SimpleData;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

/**
 * 
 * @author qiaoning
 *
 */
public class TestORMLiteActivity extends BaseTestActivity<SimpleData> {
	
	public static final void launch(final Context context) {
		final Intent intent = new Intent(context, TestORMLiteActivity.class);
		context.startActivity(intent);
	}
	
	private ORMLiteDBHelper dbHelper = null;
	private RuntimeExceptionDao<SimpleData, ?> dao = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ormlite);
		getORMLiteDBHelper();
		dao = getORMLiteDBHelper().getRuntimeExceptionDao(SimpleData.class);
		new PerformanceTestTask().execute();
	}
	
	@Override
	protected void deleteAll() {
		getORMLiteDBHelper().getWritableDatabase().execSQL("DELETE FROM simpledata");
	}

	@Override
	protected List<SimpleData> prepareTestData(final int count) {
		final List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
		for(int i = 0; i < count; i++) {
			final SimpleData simpleData = new SimpleData();
			simpleData.setName("Name" + i);
			simpleDatas.add(simpleData);
		}
		return simpleDatas;
	}

	@Override
	protected void oneByOneInsert(SimpleData object) {
		dao.create(object);
	}

	@Override
	protected void oneByOneUpdate(SimpleData object) {
		dao.update(object);
	}

	@Override
	protected void batchInsert(final List<SimpleData> objects) {
		dao.callBatchTasks(new Callable<Void>() {
			
			@Override
			public Void call() throws Exception {
				for(SimpleData simpleData : objects) {
					dao.create(simpleData);
				}
				return null;
			}
		});
	}

	@Override
	protected void batchUpdate(final List<SimpleData> objects) {
		dao.callBatchTasks(new Callable<Void>() {
			
			@Override
			public Void call() throws Exception {
				for(SimpleData simpleData : objects) {
					dao.update(simpleData);
				}
				return null;
			}
		});
	}

	@Override
	protected void reload() {
		final List<SimpleData> tempSimpleDatas = dao.queryForAll();
	}
	
	protected ORMLiteDBHelper getORMLiteDBHelper() {
		if(dbHelper == null) {
			dbHelper = OpenHelperManager.getHelper(TestORMLiteActivity.this, ORMLiteDBHelper.class);
		}
		return dbHelper;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dbHelper != null) {
			OpenHelperManager.releaseHelper();
			dbHelper = null;
		}
	}

	@Override
	protected String getLogTagName() {
		return "ORMLite";
	}
	
}
