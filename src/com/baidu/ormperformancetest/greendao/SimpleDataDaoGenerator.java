package com.baidu.ormperformancetest.greendao;

import java.io.IOException;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class SimpleDataDaoGenerator {

	public static void main(String[] args) throws IOException, Exception {
		final Schema schema = new Schema(1, "com.baidu.ormperformancetest.greendao");
		
		addSimpleData(schema);
		
		new DaoGenerator().generateAll(schema, "/Users/baidu/Documents/work/Workspaces/luna_workspace/ORMPerformanceTest/src");
	}
	
	private static void addSimpleData(final Schema schema) {
		final Entity entity = schema.addEntity("SimpleData");
		entity.addIdProperty().autoincrement();
		entity.addStringProperty("name").notNull();
	}
}
