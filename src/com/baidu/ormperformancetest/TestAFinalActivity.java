package com.baidu.ormperformancetest;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalDb;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.baidu.ormperformancetest.finaldb.SimpleData;

public class TestAFinalActivity extends BaseTestActivity<SimpleData> {
	
	public static final void launch(final Context context) {
		final Intent intent = new Intent(context, TestAFinalActivity.class);
		context.startActivity(intent);
	}
	
	private FinalDb db = null;
	private SQLiteDatabase mSQLiteDatabase;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_afinal);
		
		db = FinalDb.create(TestAFinalActivity.this, false);
		
		mSQLiteDatabase = db.getSqLiteDatabase();
		
		new PerformanceTestTask().execute();
	}

	@Override
	protected void deleteAll() {
		db.deleteAll(SimpleData.class);
	}

	@Override
	protected List<SimpleData> prepareTestData(int count) {
		final List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
		for(int i = 0; i < count; i++) {
			final SimpleData simpleData = new SimpleData();
			simpleData.setName("Name" + i);
			simpleDatas.add(simpleData);
		}
		return simpleDatas;
	}

	@Override
	protected void oneByOneInsert(SimpleData object) {
		db.saveBindId(object);
	}

	@Override
	protected void oneByOneUpdate(SimpleData object) {
		db.update(object);
	}

	@Override
	protected void batchInsert(List<SimpleData> objects) {
		mSQLiteDatabase.beginTransaction();
		final int size = objects.size();
		for(int i = 0; i < size; i++) {
			db.saveBindId(objects.get(i));
		}
		mSQLiteDatabase.setTransactionSuccessful();
		mSQLiteDatabase.endTransaction();
	}

	@Override
	protected void batchUpdate(List<SimpleData> objects) {
		mSQLiteDatabase.beginTransaction();
		final int size = objects.size();
		for(int i = 0; i < size; i++) {
			db.update(objects.get(i));
		}
		mSQLiteDatabase.setTransactionSuccessful();
		mSQLiteDatabase.endTransaction();
	}

	@Override
	protected void reload() {
		final List<SimpleData> datas = db.findAll(SimpleData.class);
	}

	@Override
	protected String getLogTagName() {
		return "FinalDB";
	}

}
