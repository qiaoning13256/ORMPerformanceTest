package com.baidu.ormperformancetest.ormlite;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "simpledata")
public class SimpleData {

	@DatabaseField(generatedId = true)
	private long id;
	@DatabaseField(canBeNull = false)
	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
