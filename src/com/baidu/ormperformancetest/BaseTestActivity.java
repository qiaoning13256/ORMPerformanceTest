package com.baidu.ormperformancetest;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.TextUtils;

public abstract class BaseTestActivity<T> extends Activity {

	protected class PerformanceTestTask extends AsyncTask<Void, Void, Void> {
		
		private ProgressDialog dialog = null;
		private long oneByOneInsertTotalTime = 0L, oneByOneUpdateTotalTime = 0L, batchInsertTotalTime = 0L, batchUpdateTotalTime = 0L, reloadTotalTime = 0L;
		
		private String logTagName = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = ProgressDialog.show(BaseTestActivity.this, getResources().getString(R.string.please_wait), getResources().getString(R.string.running_test));
			dialog.setCancelable(false);
			logTagName = BaseTestActivity.this.getLogTagName();
			if(TextUtils.isEmpty(logTagName)) {
				logTagName = "Default";
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			final int WHILE_COUNT = getTestCount();
			final int[] TEST_RECORD_NUM = getRecordCountArray();
			for(int k = 0; k < TEST_RECORD_NUM.length; k++) {
				oneByOneInsertTotalTime = 0L; oneByOneUpdateTotalTime = 0L; batchInsertTotalTime = 0L; batchUpdateTotalTime = 0L; reloadTotalTime = 0L;
				for(int j = 0; j < WHILE_COUNT; j++) {
					deleteAll();//删除之前的数据
					// 准备测试数据
					final List<T> testData = prepareTestData(TEST_RECORD_NUM[k]);
					System.gc();
					if(TEST_RECORD_NUM[k] <= 100) {//单个插入执行时间太长，所以只执行100条以内的。
						oneByOneInsertAndUpdate(testData);
						System.gc();
						deleteAll();
					}
					batchInsertAndUpdate(testData);
					System.gc();
					reload();
				}
				System.out.println("[" + logTagName + "] Avg time of " + TEST_RECORD_NUM[k] + " records is (one-by-one insert) : " + oneByOneInsertTotalTime / WHILE_COUNT);
				System.out.println("[" + logTagName + "] Avg time of " + TEST_RECORD_NUM[k] + " records is (one-by-one update) : " + oneByOneUpdateTotalTime / WHILE_COUNT);
				System.out.println("[" + logTagName + "] Avg time of " + TEST_RECORD_NUM[k] + " records is (batch insert) : " + batchInsertTotalTime / WHILE_COUNT);
				System.out.println("[" + logTagName + "] Avg time of " + TEST_RECORD_NUM[k] + " records is (batch update) : " + batchUpdateTotalTime / WHILE_COUNT);
				System.out.println("[" + logTagName + "] Avg time of " + TEST_RECORD_NUM[k] + " records is (reload) : " + reloadTotalTime / WHILE_COUNT);
			}
			return null;
		}
		
		private void oneByOneInsertAndUpdate(final List<T> testData) {
			long start = 0L, cost = 0L;
			final int size = testData.size();
			//Test insert
			start = System.currentTimeMillis();
			for(int i = 0; i < size; i++) {
				BaseTestActivity.this.oneByOneInsert(testData.get(i)); //随着List的增大，List查找对象本身也会耗费相当可观的时间
			}
			cost = System.currentTimeMillis() - start;
			oneByOneInsertTotalTime += cost;
			//Test update
			start = System.currentTimeMillis();
			for(int i = 0; i < size; i++) {
				BaseTestActivity.this.oneByOneUpdate(testData.get(i)); //随着List的增大，List查找对象本身也会耗费相当可观的时间
			}
			cost = System.currentTimeMillis() - start;
			oneByOneUpdateTotalTime += cost;
		}
		
		private void batchInsertAndUpdate(final List<T> testDatas) {
			long start = 0L, cost = 0L;
			//Test insert
			start = System.currentTimeMillis();
			BaseTestActivity.this.batchInsert(testDatas);
			cost = System.currentTimeMillis() - start;
			batchInsertTotalTime += cost;
			//Test update
			start = System.currentTimeMillis();
			BaseTestActivity.this.batchUpdate(testDatas);
			cost = System.currentTimeMillis() - start;
			batchUpdateTotalTime += cost;
		}
		
		private void reload() {
			final long start = System.currentTimeMillis();
			BaseTestActivity.this.reload();
			final long cost = System.currentTimeMillis() - start;
			reloadTotalTime += cost;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			System.out.println("All test done.");
		}
	}
	
	/**
	 * 获取实验次数
	 * @return
	 */
	protected int getTestCount() {
		return 10;
	}
	
	protected int[] getRecordCountArray() {
		return new int[]{ 1, 10, 100, 1000, 10000, 100000 };	//100w的不做测试，OOM
	}
	
	protected abstract void deleteAll();
	
	protected abstract List<T> prepareTestData(final int count);
	
	protected abstract void oneByOneInsert(final T object);
	
	protected abstract void oneByOneUpdate(final T object);
	
	protected abstract void batchInsert(final List<T> objects);
	
	protected abstract void batchUpdate(final List<T> objects);
	
	protected abstract void reload();
	
	protected abstract String getLogTagName();
}
