package com.baidu.ormperformancetest.origin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OriginDBHelper extends SQLiteOpenHelper {
	
	private static final String DB_NAME = "origin-db";
	private static final int DB_VERSION = 1;
	
	public static final String TABLE_NAME = "simple_data";

	public OriginDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + " ( id INTEGER PRIMARY KEY AUTOINCREMENT,name )");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
