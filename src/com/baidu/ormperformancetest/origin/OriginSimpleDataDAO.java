package com.baidu.ormperformancetest.origin;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class OriginSimpleDataDAO {
	
	private Context context;

	private OriginDBHelper dbHelper;
	private SQLiteDatabase db;
	
	private static final String INSERT_TEMPLATE_SQL = "INSERT INTO simple_data(name) VALUES( ? )";
	private static final String UPDATE_TEMPLATE_SQL = "UPDATE simple_data SET name = ? WHERE id = ?";
	private SQLiteStatement insertStatement, updateStatement;
	
	public OriginSimpleDataDAO(Context context) {
		this.context = context;
		this.dbHelper = new OriginDBHelper(this.context);
		this.db = this.dbHelper.getWritableDatabase();
		
		this.insertStatement = this.db.compileStatement(INSERT_TEMPLATE_SQL);
		this.updateStatement = this.db.compileStatement(UPDATE_TEMPLATE_SQL);
	}
	
	public void insert(final SimpleData simpleData) {
		final ContentValues cv = new ContentValues();
		cv.put("name", simpleData.getName());
		final long rowId = db.insert(OriginDBHelper.TABLE_NAME, null, cv);
		simpleData.setId(rowId);
	}
	
	public void insertStatement(final SimpleData simpleData) {
		this.insertStatement.clearBindings();
		this.insertStatement.bindString(1, simpleData.getName());
		final long rowId = this.insertStatement.executeInsert();
		simpleData.setId(rowId);
	}
	
	public void update(final SimpleData simpleData) {
		final ContentValues cv = new ContentValues();
		cv.put("name", simpleData.getName());
		db.update(OriginDBHelper.TABLE_NAME, cv, " id = ? ", new String[]{ simpleData.getId() + "" });
	}
	
	@SuppressLint("NewApi")
	public void updateStatement(final SimpleData simpleData) {
		this.updateStatement.clearBindings();
		this.updateStatement.bindString(1, simpleData.getName());
		this.updateStatement.bindLong(2, simpleData.getId());
		this.updateStatement.executeUpdateDelete();
	}
	
	public void insertAll(final List<SimpleData> simpleDatas) {
		db.beginTransaction();
		for(SimpleData simpleData : simpleDatas) {
			insert(simpleData);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public void insertAllStatement(final List<SimpleData> simpleDatas) {
		db.beginTransaction();
		for(SimpleData simpleData : simpleDatas) {
			insertStatement(simpleData);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public void updateAll(final List<SimpleData> simpleDatas) {
		db.beginTransaction();
		for(SimpleData simpleData : simpleDatas) {
			update(simpleData);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public void updateAllStatement(final List<SimpleData> simpleDatas) {
		db.beginTransaction();
		for(SimpleData simpleData : simpleDatas) {
			updateStatement(simpleData);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public List<SimpleData> loadAll() {
		final List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
		final Cursor cursor = db.query(OriginDBHelper.TABLE_NAME, null, null, null, null, null, null);
		if(cursor != null && cursor.moveToFirst()) {
			do {
				final SimpleData simpleData = new SimpleData();
				final int id = cursor.getInt(cursor.getColumnIndex("id"));
				final String name = cursor.getString(cursor.getColumnIndex("name"));
				simpleData.setId(id);
				simpleData.setName(name);
				simpleDatas.add(simpleData);
			} while(cursor.moveToNext());
			cursor.close();
		}
		return simpleDatas;
	}
	
	public void deleteAll() {
		db.delete(OriginDBHelper.TABLE_NAME, null, null);
	}
}
