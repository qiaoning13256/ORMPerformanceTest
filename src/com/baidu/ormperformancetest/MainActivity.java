package com.baidu.ormperformancetest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * 
 * @author qiaoning
 *
 */
public class MainActivity extends Activity {
	
	private Button ormliteButton, greenDaoButton, finalDBButton, originButton;
	
	private OnButtonClickListener onButtonClickListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		onButtonClickListener = new OnButtonClickListener();
		
		ormliteButton = (Button) findViewById(R.id.ormliteButton);
		greenDaoButton = (Button) findViewById(R.id.greenDaoButton);
		finalDBButton = (Button) findViewById(R.id.finalDBButton);
		originButton = (Button) findViewById(R.id.originButton);
		
		ormliteButton.setOnClickListener(onButtonClickListener);
		greenDaoButton.setOnClickListener(onButtonClickListener);
		finalDBButton.setOnClickListener(onButtonClickListener);
		originButton.setOnClickListener(onButtonClickListener);
	}
	
	private class OnButtonClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ormliteButton:
				TestORMLiteActivity.launch(MainActivity.this);
				break;
			case R.id.greenDaoButton:
				TestGreenDAOActivity.launch(MainActivity.this);
				break;
			case R.id.finalDBButton:
				TestAFinalActivity.launch(MainActivity.this);
				break;
			case R.id.originButton:
				TestOriginActivity.launch(MainActivity.this);
				break;
			default:
				break;
			}
		}
		
	}
}
