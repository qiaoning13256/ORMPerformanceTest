ORMPerformanceTest
===================

----------

Documents
--------------
#### 关联项目
-  https://git.oschina.net/qiaoning13256/AFinal.git (Project关联）
- https://git.oschina.net/qiaoning13256/DaoGenerator.git (Project关联）
- https://git.oschina.net/qiaoning13256/appcompat_v7.git (lib关联）

#### 关于实验结果的说明

- Android模拟器 测试环境：
>-
操作系统：Mac OS X 10.10
虚拟机：Genymotion Version 2.3.1
模拟器Android版本：4.2.2 API17  vs  2.3.7 API10
SQLite版本：3.7.11 2012-03-20 11:35:50  vs  3.6.22
真机：
小米3 Android 4.4.4 Sqlite Version 3.7.11
HTC MyTouch 4G（Glacier）

- 所有实验结果数据为取10次的平均值


- 单个插入（更新）（one-by-one insert/update）只实验了100条以下（含100条）的数据（节省时间，实际开发项目中，大量的插入数据会使用事务，单个插入太多没有实际意义）


- 实验结果中，ORMLite与ORMLite2的区别：
>- ORMLite使用注解（Annotation）
>- ORMLite2使用res/raw/目录下的配置文件

- 与AFinal的作者联系了一下，确认FinalDB不支持事务，实验结果中的批量插入，是自己稍微改动了一下AFinal的源码，开启事务后 的实验结果。


- 模拟器中操作，使用GreenDAO，Android 2.3.7，10w次实验时报了一个OOM错误，原因未知。实验数据为手工运行10次取的平均值。模拟器Android4.2.2没有出现OOM错误。


- 使用Android原生的数据库操作API，批量插入中，有一项为“参数绑定”，参数绑定中，更新操作（update）API最低系统要求为11。使用参数绑定会大大节省插入，更新时间。但是API Level低于11的Android版本不能使用更新操作的API（Insert的可以）。


- 使用Android原生的数据库操作API，单个插入/更新（one-by-one insert/update）时，使用参数绑定 与 不适用参数绑定，执行所需时间几乎一样，所以实验结果中，one-by-one插入/更新时，没有“参数绑定”这行


- 使用Android原生的数据库操作API，select数据时，没有参数绑定这么一说，所以“加载”测试项，也没有“参数绑定”这行


- 为了保证实验测试条件的一致性，没有使用标志位判断。
>- ORMLite与ORMLite2需要手动修改代码。
>- 原生API与原生API（参数绑定）同样需要手动修改代码


#### 实验结果

##### MI3，Android 4.4.4，Sqlite V3.7.11

- 10万条记录批量插入更新（MI3，Android 4.4.4，Sqlite V3.7.11）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/mi3_iu_10w.png)

- 1万条记录批量插入更新（MI3，Android 4.4.4，Sqlite V3.7.11）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/mi3_iu_1w.png)

- 1000条记录批量插入更新（MI3，Android 4.4.4，Sqlite V3.7.11）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/mi3_iu_1000.png)

- 加载10万条记录用时（MI3，Android 4.4.4，Sqlite V3.7.11）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/mi3_load_10w.png)

- 加载1万条记录用时（MI3，Android 4.4.4，Sqlite V3.7.11）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/mi3_load_1w.png)

------
##### HTC Glacier ，Android 2.3.7，Sqlite V3.7.2

- 10万条记录批量插入更新（HTC Glacier ，Android 2.3.7，Sqlite V3.7.2）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/htc_glacier_iu_10w.png)

- 1万条记录批量插入更新（HTC Glacier ，Android 2.3.7，Sqlite V3.7.2）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/htc_glacier_iu_1w.png)

- 1000条记录批量插入更新（HTC Glacier ，Android 2.3.7，Sqlite V3.7.2）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/htc_glacier_iu_1000.png)

- 加载10万条记录用时（HTC Glacier ，Android 2.3.7，Sqlite V3.7.2）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/htc_glacier_load_10w.png)

- 加载1万条记录用时（HTC Glacier ，Android 2.3.7，Sqlite V3.7.2）


![](http://git.oschina.net/qiaoning13256/ORMPerformanceTest/raw/master/doc/chart_result/htc_glacier_load_1w.png)